const gulp = require('gulp');
// const sass = require('gulp-sass');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function() {
    return gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', function(error){
            console.log( error );
        }))
        .pipe(cssnano())
        .pipe(autoprefixer({
	        overrideBrowserslist: ['last 16 versions'],
          cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css/'));
});
gulp.task('sass:watch', function() {
    gulp.watch('./scss/*.scss', gulp.series('sass'));
});
