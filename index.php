<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>VACUUM</title>
	<meta name="description" content="December 17|19,2021 - Kyiv, Ukraine"/>
	<meta property="og:title" content="Techstars Startup Weekend Kyiv"/>
	<meta property="og:description" content="December 17|19,2021 - Kyiv, Ukraine"/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content="./img/og-image.png"/>

	<link href="./css/firstScreen.css" rel="stylesheet">

</head>

<body>

	<header>
		<div class="menu">
			<? $menuItemsAnchors = ['accelerator','benefits','mentors','partners','contact'];
			$menuItemsTitles = ['Accelerator','Key benefits','Mentors','Partners','Contact'];
			for ($i=0; $i < count($menuItemsAnchors); $i++) { ?> 
				<div class="item">
					<a href="#<?=$menuItemsAnchors[$i]?>"><?=$menuItemsTitles[$i]?></a>
				</div>
			<? } ?>
		</div>
	</header>


	<section class="hero">
		<div class="wrapper">
			<? include('img/banner.svg') ?>
			
			<div class="request">
				<div class="text">Конвертуй науку в бізнес</div>
				<div class="button">Подай заявку</div>
			</div>
		</div>
	</section>

	<section id="accelerator">
		<div>
			<h2>Accelerator</h2>
			<h3>VACUUM</h3>
			<p>Тримісячна акселераційна програма, яка розроблена спільно YEP та Home of Startups для українських наукоємних проєктів із потенціалом для комерціалізації. </p>
			
		</div>
		<div>
			<img src="img/accelerator.jpg" />
			<div class="infobox">
				
				<div>
					<svg width="24" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M0 7C0 3.13 3.13 0 7 0C10.87 0 14 3.13 14 7C14 12.25 7 20 7 20C7 20 0 12.25 0 7ZM4.5 7C4.5 8.38 5.62 9.5 7 9.5C8.38 9.5 9.5 8.38 9.5 7C9.5 5.62 8.38 4.5 7 4.5C5.62 4.5 4.5 5.62 4.5 7Z" fill="#F29900"/>
					</svg>
					<span class="desc">Гібридний формат (офлайн та онлайн)</span>
				</div>
				
				<div>
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 11H7V13H9V11ZM13 11H11V13H13V11ZM17 11H15V13H17V11ZM19 4H18V2H16V4H8V2H6V4H5C3.89 4 3.01 4.9 3.01 6L3 20C3 21.1 3.89 22 5 22H19C20.1 22 21 21.1 21 20V6C21 4.9 20.1 4 19 4ZM19 20H5V9H19V20Z" fill="#F29900"/>
					</svg>
					<span class="desc">3 місяці</span>
				</div>
				
				<div>
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 2C6.48 2 2 6.48 2 12C2 17.52 6.48 22 12 22C17.52 22 22 17.52 22 12C22 6.48 17.52 2 12 2ZM13 17H11V11H13V17ZM13 9H11V7H13V9Z" fill="#F29900"/>
					</svg>
					<span class="desc">від ідеї до прототипу</span>
				</div>
				
			</div>
		</div>
	</section>



	<section id="benefits">
		<h2>Key benefits</h2>
		
		<div class="benefitsWrapper">
			<? $benefitsAliases = ['free','protection','mentoring','tools','validation','experts'];
			$benefitsTitles = [
				'Безкоштовно',
				'Юридичний захист',
				'Індивідуальне менторство',
				'Доступ до фінансових та нефінансових інструментів',
				'Технологічна та бізнес валідація',
				'Доступ до експертів' ];
			$benefitsDescs = [
				'Вам не потрібно платити чи віддавати частку для участі у програмі',
				'Ваші ідеї та розробки захищені на всіх етапах акселерації',
				'Ментори підібрані під потреби Вашого проєкту',
				'Допомога із залученням фінансових та/або нефінансових ресурсів для розвитку Вашого проєкту',
				'Гарантований зворотній зв’язок від бізнесу та технологічних експертів щодо Вашої ідеї',
				'Доступ до широкої мережі технологічних і бізнес експертів з усього світу' ];
			for ($i=0 ; $i < count($benefitsAliases); $i++) { ?>
				<div class="benefit">
					<? include('img/benefits/'.$benefitsAliases[$i].'.svg') ?>
					<p class="title" <? if ($i === 5) { ?> style="margin-bottom: 28px" <? } ?> ><?=$benefitsTitles[$i]?></p>
					<p class="desc"><?=$benefitsDescs[$i]?></p>
				</div>
			<? } ?>
		</div>
	</section>


	<section id="mentors">
		<h2>Mentors</h2>
		
		<div class="experts-wrapper">
			<? $experts = [
				'Elena Donets',
				'Maret Ahonen',
				'Serhii Martynchuk',
				'Alyona Kalibaba',
				'Oleksandr Liashenko',
				'Roman Kirigetov',
				'Denis Zernyshkin',
				'Andrii Manuilov',
				'Valeriia Holierova',
				'Oleksandr Davydenko' ];
			$expertDescs = [
				'Co-founder and COO at Spyre Group',
				'sTARTUp Lab Manager at the University of Tartu',
				'General Manager at Cisco, Managing Partner at UATECH',
				'Co-founder of SILab Ukraine',
				'Vice President of Technology at OKKO',
				'Former CEO & Co-founder at Kabanchik.ua, CEO at FlashBeats','CEO at Gravitec.net & Advisory Board at Piar.io',
				'Co-founder & CEO IOON',
				'Product Manager at Empath',
				'Chief Innovation Officer at TECHIIA, 
				Managing Director of the VRTX Venture Lab' ];
			$expertSocial = ['LinkedIn','LinkedIn','LinkedIn','LinkedIn','Facebook','Facebook','Facebook','Facebook','LinkedIn','Facebook'];
			$expertURLs = [
				'https://www.linkedin.com/in/elenadonets/',
				'https://www.linkedin.com/in/maret-ahonen-36379618/?originalSubdomain=ee',
				'https://www.linkedin.com/in/sergey-martynchuk-5a31411/',
				'https://www.linkedin.com/in/kalibabaalena/',
				'https://www.facebook.com/lyashenko.aleksandr',
				'https://www.facebook.com/kirigetov',
				'https://www.facebook.com/denis.zernyshkin',
				'https://www.facebook.com/andrew.manuilov.7',
				'https://www.linkedin.com/in/valeriiaholierova/',
				'https://www.facebook.com/olekdav']; 
			for ($i=0 ; $i < count($experts); $i++) { ?>
				<div class="experts-box">
					<div class="experts-photo">
						<img src="img/mentors/<?=str_replace(' ','_',$experts[$i])?>.jpg" />
						<div class="experts-overlay"></div>
					</div>
					
					<div class="experts-information">
						
						<div class="experts-name"><?=$experts[$i]?></div>
						<div class="experts-description">
							<p><?=$expertDescs[$i]?></p>
							<a target="_blank" href="<?=$expertURLs[$i]?>">
								<? include( $expertSocial[$i] === 'Facebook' ? 'img/facebook.svg' : 'img/twitter.svg') ?>
							</a>
						</div>
					</div>
				</div>
			<? } ?>
		</div>
	</section>
	
	
	<section id="partners">
		<h2>Partners</h2>
		
		<div class="partners">
			<? $partners = ['techiia','usf','mon'];
			$partnersURLs = ['https://techiia.com/ua/', 'https://usf.com.ua', 'https://mon.gov.ua/ua'];
			for ($i=0; $i < count($partners); $i++) { ?>
				<a target="_blank" href="<?=$partnersURLs[$i]?>"><img src="img/partners/<?=$partners[$i]?>.png" /></a>
			<? } ?>
		</div>
	</section>
	
	
	<section id="contact">
		<h2>Contact</h2>
		
		<form method="post" action="action.php">
			<div>
				<label for="name">ПІБ заявника:</label>
				<input id="name" name="name" type="text" />
			</div>
			
			<div>
				<label for="project">Назва проєкту:</label>
				<input id="project" name="project" type="text" />
			</div>
			
			<div>
				<label for="email">E-mail:</label>
				<input id="email" name="email" type="email" />
			</div>
			
			<div>
				<label for="tel">Телефон:</label>
				<input id="tel" name="tel" type="tel" />
			</div>
			
			<div>
				<label for="website" style="padding-bottom: 23px">Вебсайт проєкту, якщо такий є:</label>
				<input id="website" name="website" type="text" />
			</div>
			
			<div>
				<label for="institute ">Вкажіть назву наукової установи чи університету, якщо Ви там працюєте/навчаєтесь:</label>
				<input id="institute" name="institute " type="text" />
			</div>
			
			<div class="area">
				<label for="desc">Короткий опис проєкту:</label>
				<textarea id="desc" name="project"></textarea>
			</div>
			
			<div class="area">
				Чи є у вас команда?
				<input type="radio" id="yes" name="team" value="yes" checked>
				<label for="yes">Так</label>
				<input type="radio" id="no" name="team" value="no">
				<label for="no">Ні</label>
			</div>
			
			<div class="area">
				<label for="teamDesc">Якщо у Вас є команда – опишіть її коротко:</label>
				<textarea id="teamDesc" name="teamDesc"></textarea>
			</div>
			
			<div class="area">
				<div class="area">Тут Ви можете додати будь-які презентаційні матеріали до 10 мб:</div>
				<input class="form-control" type="file" id="formFileMultiple" multiple>
			</div>
			
			<div>
				<input type="checkbox" id="agree" name="agree" />
				<label for="agree">Надаю згоду на обробку персональних даних</label>
			</div>
			
			<div>
				<input type="submit" value="Відправити" />
			</div>
		</form>
	</section>


	<footer></footer>

	<div class="goTop">
		<? include('img/goTop.svg') ?>
	</div>

	<link href="./css/othersScreens.css" rel="stylesheet">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="./js/custom.js" type="text/javascript"></script>


</body>
</html>